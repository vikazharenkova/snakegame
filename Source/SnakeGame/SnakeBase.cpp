// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.3;
	MovementSpeedUp = 0.05;
	MovementSpeedDown = 0.7;
	TimeOfActionBonuses = 8.f;
	TimeSpeed = 0.f;
	WallsBonusesInteract = false;
	BonusesTime = 0.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	MovementSpeedDefault = MovementSpeed;
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MovementSpeed == MovementSpeedUp)
	{		
		TimeSpeed = TimeSpeed + DeltaTime;
	}

	if (MovementSpeed == MovementSpeedDown)
	{
		TimeSpeed = TimeSpeed + DeltaTime;
	}

	if (WallsBonusesInteract == true)
	{
		BonusesTime = BonusesTime + DeltaTime;
		for (int i = 1; i < SnakeElements.Num(); i++)
		{
			SnakeElements[i]->SetElementBonuseType();
		}
	}

	if (TimeSpeed > TimeOfActionBonuses)
	{
		MovementSpeed = MovementSpeedDefault;
		SetActorTickInterval(MovementSpeed);
		TimeSpeed = 0.f;
	}

	if (BonusesTime > TimeOfActionBonuses)
	{
		WallsBonusesInteract = false;
		BonusesTime = 0.f;
		for (int i = 1; i < SnakeElements.Num(); i++)
		{
			SnakeElements[i]->ReturnElementType();
		}		
	}
	//SetActorTickInterval(MovementSpeed);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		NewSnakeElem->SetActorHiddenInGame(false);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}		
	}

	if (SnakeElements.Num() > 1)
	{
		SnakeElements[SnakeElements.Num() - 1]->SetActorHiddenInGame(true);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;

	switch(LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		if (SnakeElements[i]->IsHidden())
		{
			SnakeElements[i]->SetActorHiddenInGame(false);
		}
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
		SetActorTickInterval(MovementSpeed);
	}
}

