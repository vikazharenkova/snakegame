// Fill out your copyright notice in the Description page of Project Settings.


#include "Walls.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
AWalls::AWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AWalls::BeginPlay()
{
	Super::BeginPlay();

	WallsMaterial = MeshComponent->GetMaterial(0);
}

// Called every frame
void AWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWalls::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		auto Material = MeshComponent->GetMaterial(0);
		if (IsValid(Snake) && Material == WallsMaterial)
		{
			Snake->Destroy(true, true);
		}
		if (IsValid(Snake) && Material != WallsMaterial && Snake->WallsBonusesInteract == false)
		{
			Snake->Destroy(true, true);
		}
	}
}

void AWalls::SetWallsType_Implementation()
{

}

