// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
//#include "Interactable.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ABonuses;
class AWalls;
class AWallsBonuses;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize = 50.f;

	//Food
	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodElemClass;

	//Bonuses
	UPROPERTY(BlueprintReadWrite)
	ABonuses* BonusesActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonuses> BonusesElemClass;

	//WallsBonuses
	UPROPERTY(BlueprintReadWrite)
	AWallsBonuses* WallsBonusesActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWallsBonuses> WallsBonusesElemClass;

	UPROPERTY(EditDefaultsOnly)
	float CreationWallsTime = 0.f;
	float DestroyWallsTime;
	float DestroyTime = 10.f;

	//Walls
	UPROPERTY(BlueprintReadWrite)
	AWalls* WallsActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWalls> WallsElemClass;

	//Field size
	UPROPERTY(EditDefaultsOnly)
	float MinX = -1130.f;

	UPROPERTY(EditDefaultsOnly)
	float MaxX = 1130.f;

	UPROPERTY(EditDefaultsOnly)
	float MinY = -1830.f;

	UPROPERTY(EditDefaultsOnly)
	float MaxY = 1830.f;

	UPROPERTY(EditDefaultsOnly)
	float AverZ = 25.f;

	// Time Delay
	UPROPERTY(EditDefaultsOnly)
	float MinTimeDelay = 5.f;
	float MaxTimeDelay = 200.f;

	UPROPERTY(EditDefaultsOnly)
	float MinTimeDelay2 = 15.f;
	float MaxTimeDelay2 = 200.f;

	UPROPERTY(EditDefaultsOnly)
	float BufferTime = 0.f;
	float BufferTime2 = 0.f;
	float BufferTime3 = 0.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	//Food adding
	UFUNCTION()
	void AddFoodBonuses(float DeltaTime);

	//Walls adding
	UFUNCTION()
	void AddWalls();

};
