// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Bonuses.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ABonuses : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonuses();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	float BufferTime = 0;

	UPROPERTY(EditDefaultsOnly)
	float DestroyTime = 10.f;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* BonusesMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetBonuseType();
	void SetBonuseType_Implementation();
};
