// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Bonuses.h"
#include "Walls.h"
#include "WallsBonuses.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	AddWalls();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddFoodBonuses(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::AddFoodBonuses(float DeltaTime)
{
	float AverX = FMath::FRandRange(MinX + ElementSize*2, MaxX - ElementSize*2);
	float AverY = FMath::FRandRange(MinY + ElementSize*2, MaxY- ElementSize*2);
	FVector StartPoint(AverX, AverY, AverZ);
	FTransform StartPointTransform(StartPoint);
	if (AverY != MinY + (MaxY - MinY) / 5 || AverY != (MinY + (MaxY - MinY) / 5) * 2 || AverY != (MinY + (MaxY - MinY) / 5) * 3 || AverY != (MinY + (MaxY - MinY) / 5) * 4)
	{
		if (IsValid(SnakeActor))
		{
			if (IsValid(FoodActor) != true)
			{
				BufferTime += DeltaTime;
				float TimeDelay = FMath::FRandRange(MinTimeDelay, MaxTimeDelay);
				if (BufferTime > TimeDelay)
				{
					FoodActor = GetWorld()->SpawnActor<AFood>(FoodElemClass, StartPointTransform);
					BufferTime = 0;
				}
			}

			if (IsValid(BonusesActor) != true)
			{
				BufferTime2 += DeltaTime;
				float TimeDelay2 = FMath::FRandRange(MinTimeDelay2, MaxTimeDelay2);
				if (BufferTime2 > TimeDelay2)
				{
					int32 Random = FMath::FRandRange(1, 5000);
					BonusesActor = GetWorld()->SpawnActor<ABonuses>(BonusesElemClass, StartPointTransform);
					BufferTime2 = 0;
					if (Random % 2 == 1)
					{
						BonusesActor->SetBonuseType();
					}
				}

			}
			if (IsValid(WallsBonusesActor) != true)
			{
				BufferTime3 += DeltaTime;
				float TimeDelay3 = FMath::FRandRange(MinTimeDelay2, MaxTimeDelay2);
				if (BufferTime3 > TimeDelay3)
				{
					WallsBonusesActor = GetWorld()->SpawnActor<AWallsBonuses>(WallsBonusesElemClass, StartPointTransform);
					BufferTime3 = 0;
				}
			}
		}
	}
	}


void APlayerPawnBase::AddWalls()
{
	if (IsValid(SnakeActor))
	{
		for (float CoordinateX = MinX; CoordinateX <= MaxX; CoordinateX += ElementSize)
		{
			FVector StartPoint(CoordinateX, MinY, AverZ);
			FTransform StartPointTransform(StartPoint);
			WallsActor = GetWorld()->SpawnActor<AWalls>(WallsElemClass, StartPointTransform);
			FVector StartPoint2(CoordinateX, MaxY, AverZ);
			FTransform StartPointTransform2(StartPoint2);
			WallsActor  = GetWorld()->SpawnActor<AWalls>(WallsElemClass, StartPointTransform2);
		}
		for (float CoordinateY = MinY; CoordinateY <= MaxY; CoordinateY += ElementSize)
		{
			FVector StartPoint(MinX, CoordinateY, AverZ);
			FTransform StartPointTransform(StartPoint);
			WallsActor = GetWorld()->SpawnActor<AWalls>(WallsElemClass, StartPointTransform);
			FVector StartPoint2(MaxX, CoordinateY, AverZ);
			FTransform StartPointTransform2(StartPoint2);
			WallsActor = GetWorld()->SpawnActor<AWalls>(WallsElemClass, StartPointTransform2);
		}

		for (float CoordinateY = MinY + (MaxY - MinY) / 5; CoordinateY <= MaxY - ElementSize; CoordinateY += ((MaxY - MinY) / 5) * 2)
		{
			for (float CoordinateX = MinX + (MaxX - MinX) / 4; CoordinateX <= MaxX - (MaxX - MinX)/2; CoordinateX += ElementSize)
			{
				FVector StartPoint(CoordinateX, CoordinateY, AverZ);
				FTransform StartPointTransform(StartPoint);
				WallsActor = GetWorld()->SpawnActor<AWalls>(WallsElemClass, StartPointTransform);
				WallsActor->SetWallsType();
			}
		}

		for (float CoordinateY = MinY + ((MaxY - MinY) / 5)*2; CoordinateY <= MaxY - ElementSize; CoordinateY += ((MaxY - MinY) / 5) * 2)
		{
			for (float CoordinateX = MinX + (MaxX - MinX) /2; CoordinateX <= MaxX - (MaxX - MinX) / 4; CoordinateX += ElementSize)
			{
				FVector StartPoint(CoordinateX, CoordinateY, AverZ);
				FTransform StartPointTransform(StartPoint);
				WallsActor = GetWorld()->SpawnActor<AWalls>(WallsElemClass, StartPointTransform);
				WallsActor->SetWallsType();
			}
		}
	}
}

