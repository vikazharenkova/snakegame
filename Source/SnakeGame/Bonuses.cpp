// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonuses.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABonuses::ABonuses()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ABonuses::BeginPlay()
{
	Super::BeginPlay();

	BonusesMaterial = MeshComponent->GetMaterial(0);
	
}

// Called every frame
void ABonuses::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BufferTime += DeltaTime;
	if (BufferTime > DestroyTime)
	{
		Destroy(true, true);
		BufferTime = 0;
	}
}

void ABonuses::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			auto Material = MeshComponent->GetMaterial(0);
			if (Material == BonusesMaterial)
			{
				Snake->MovementSpeed = Snake->MovementSpeedUp;
			}
			else
			{
				Snake->MovementSpeed = Snake->MovementSpeedDown;
			}
			Destroy(true, true);
		}
	}
}

void ABonuses::SetBonuseType_Implementation()
{

}

