// Fill out your copyright notice in the Description page of Project Settings.


#include "WallsBonuses.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AWallsBonuses::AWallsBonuses()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

}

// Called when the game starts or when spawned
void AWallsBonuses::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallsBonuses::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BufferTime += DeltaTime;
	if (BufferTime > DestroyTime)
	{
		Destroy(true, true);
		BufferTime = 0;
	}
}

void AWallsBonuses::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->WallsBonusesInteract = true;
			Destroy(true, true);
		}
	}
}

